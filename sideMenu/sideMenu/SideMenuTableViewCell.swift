//
//  SideMenuTableViewCell.swift
//  sideMenu
//
//  Created by Admin on 3/15/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var label_sideMenu: UILabel!
    @IBOutlet weak var icon_sideMenu: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
