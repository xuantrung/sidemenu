//
//  ViewController.swift
//  copyPaste_autoDeleteCopy
//
//  Created by Admin on 3/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var hiddenMenuButton: UIButton!
    @IBOutlet weak var sideMenu: UIView!
    @IBOutlet weak var MenuTableView: UITableView!
    
    var menuIsHidden = true
    var opacity = 0
    var iconArr = ["menu-icon_help","icon_help_privacy-policy", "icon_setting_change-pki-key", "icon_setting_edit-account", "icon_setting_language", "iconSettingDeviceManage", "menu-icon_settings", "menuIconSubscription","menu-icon_signout"]
    var labelArr = ["Help", "Manage Security", "Change Key", "Edit Account", "Language", "Manage Device", "Setting", "Subscribe", "Sign Out"]
    //var ViewControllerDict : [String: UIViewController] = ["Help" : HelpViewController]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MenuTableView.delegate = self
        MenuTableView.dataSource = self
        MenuTableView.bounces = false
        configureNavigationBar()
        configSwipeSideMenu()
        //configureSideMenu()

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        configureSideMenu()

    }
    
    func configureSideMenu(){
        hiddenMenuButton.isHidden = true
        sideMenu.backgroundColor = .darkGray
        self.sideMenu.center.x = -self.sideMenu.frame.size.width
    }
    @IBAction func clickHiddenMenuBtn(_ sender: Any) {
        handleMenuToggle()
    }
    
    func transferSlideMenu(){
        
        if menuIsHidden{
            UIView.animate(withDuration: 0.7) {
                self.sideMenu.center.x = -self.sideMenu.frame.size.width
                self.opacity = 0
                self.hiddenMenuButton.backgroundColor = UIColor.black.withAlphaComponent(CGFloat(self.opacity)/10)
                self.navigationController?.navigationBar.barTintColor = UIColor.darkGray.withAlphaComponent(CGFloat(self.opacity)/10)
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.7) {
                self.hiddenMenuButton.isHidden = true
            }
        }
        else{
            hiddenMenuButton.isHidden = false
            UIView.animate(withDuration: 0.7) {
                self.sideMenu.center.x = self.sideMenu.frame.size.width/2
                self.opacity = 10
                self.hiddenMenuButton.backgroundColor = UIColor.black.withAlphaComponent(CGFloat(self.opacity)/10)
                self.navigationController?.navigationBar.barTintColor = UIColor.darkGray.withAlphaComponent(CGFloat(self.opacity)/10)
            }
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            self.navigationController?.navigationBar.isHidden = !(self.navigationController?.navigationBar.isHidden)!
        }
    }
    func configSwipeSideMenu(){
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swipeMenu(sender:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swipeMenu(sender:)))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    @objc func swipeMenu(sender: UISwipeGestureRecognizer){
        let local = sender.location(in: self.view)
        switch sender.direction {
        case .right:
            //swipe from left vertical
            if local.x > 5{
                return
            }
        case .left:
            //swipe only when side menu displayed
            if menuIsHidden{
                return
            }
        default:
            return
        }
        handleMenuToggle()
    }
    
    func configureNavigationBar() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_white_3x").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleMenuToggle))
        navigationItem.title = "Slide menu"
        navigationController?.navigationBar.barTintColor = UIColor.darkGray
        navigationController?.navigationBar.barStyle = .black
        
        //        let navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 45))
        //        navBar.barTintColor = .darkGray
        //        navBar.barStyle = .black
        //        navBar.setItems([navigationItem], animated: false)
        // self.view.addSubview(navBar)
    }
    @objc func handleMenuToggle(){
        print("trung")
        menuIsHidden = !menuIsHidden
        transferSlideMenu()
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = MenuTableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuTableViewCell
        cell.icon_sideMenu.image = UIImage(named: iconArr[indexPath.row])
        cell.label_sideMenu.text = labelArr[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let destination_vc = self.storyboard!.instantiateViewController(withIdentifier: labelArr[indexPath.row])
//        switch labelArr[indexPath.row] {
//        case "Help":
//            destination_vc as! HelpViewController
//        default:
//            break
//        }
        //self.configureSideMenu()
        self.handleMenuToggle()
        //self.transferSlideMenu()
        self.navigationController?.pushViewController(destination_vc, animated: true)
    }
}
